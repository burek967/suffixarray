# README #

This is a simple implementation of parallel version of Karkkainen's algorithm (sequential version has complexity O(N)) for computing suffix array. 
It was written using thrust library. 

### Authors ###
Rafał Burczyński
Vladyslav Hlembotskyi
